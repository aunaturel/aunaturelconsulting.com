---
title: "Disclaimer"
date: 2020-07-15T21:13:19-04:00
draft: false
image: 
imageposition: 
imagequality: 
staticimage: false
description: 
calltitle: 
callbutton: 
calllink: 
private: false
---

Physicians and Staff associated with Au Naturel Health Consulting do not practice medicine with clients. They do not prescribe medicine or any medical treatment. Additionally, they will not adjust client existing medications or treatments. All entities of Au Naturel Health Consulting limit their services strictly to guiding clients through a healthy lifestyle program. All medical decisions, treatment and prescriptions are left up to the clients primary care physician. 
