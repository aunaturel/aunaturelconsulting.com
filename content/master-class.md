---
title: Join Our 5lb Challenge
date: 2020-07-09T19:47:33-04:00
draft: false
private: false
description: null
image: ""
imageposition: null
imagequality: null
staticimage: false
calltitle: null
callbutton: ""
calllink: ""
---
**JOIN OUR 5LB CHALLENGE AND MAKE 2022 YOUR BEST YEAR YET**

YOU’RE INVITED TO BE

IN A STATE OF HEALTH AND HOPE.

FIVE DAYS.

FIVE VIDEOS.

ONE FRESH START.

Do You Enjoy Having the Energy Boost You Need to Be Maximally Productive and Feel Great While You’re At It?  Are you trying to let go of excess weight and need help while doing it?

## If so this is the challenge for you!

No matter where you are in your journey, these practical tips and explanations will arm you with the tools you need to proactively take charge of your health and enjoy a more vibrant and fulfilling life!

{{< button href="/sign-up/" >}}[Sign Up Now](https://aunaturelhealth.kartra.com/page/Registration){{< /button >}}