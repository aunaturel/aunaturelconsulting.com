---
title: About Us
date: 2020-06-24T21:36:11-04:00
draft: false
private: false
description: null
image: /img/untitled-design-1-.png
imageposition: Top
imagequality: null
calltitle: Convinced you need to start your healing journey?
callbutton: Sign Up Today
calllink: /sign-up/
---

Our leadership team, with advanced degrees in Medicine, Business, Public Health and Law, take a unique and multi-faceted approach to helping you achieve and maintain health and wellness. We believe that it takes **A comprehensive and intentional approach to healthcare to achieve results that defy current health statistics**.

Our team members are recognized as leaders in the growing field of comprehensive lifestyle medicine and have been featured in the media, published articles in the medical literature, and presented in scientific conferences on **Comprehensive Innovative Approaches** to the delivery of comprehensive healthcare that challenge the existing health status quo.

While we believe that there are principles, which are generalizable, our strategy has been to use **A Personalized and Customized Approach** to help you maximize your personal health with the support and guidance of seasoned healthcare professionals That will support you every step of the way.