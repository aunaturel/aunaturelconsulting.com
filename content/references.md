---
title: "References"
date: 2020-07-15T20:51:24-04:00
draft: false
image: 
imageposition: 
imagequality: 
staticimage: false
description: 
calltitle: 
callbutton: 
calllink: 
private: false
---

{{< references >}}
<li><a href="https://hbr.org/2009/01/what-can-coaches-do-for-you" target="_blank" rel="noreferrer noopener">Coutu, Diane, and Carol Kauffman. “What Can Coaches Do for You?” Harvard Business Review, September 7, 2017.</a></li>
<li><a href="https://www.theguardian.com/world/2020/apr/09/why-do-some-young-people-die-of-coronavirus-covid-19-genes-viral-load" target="_blank" rel="noreferrer noopener">McKie, Robin. “Why Do Some Young People Die of Coronavirus?” The Guardian. Guardian News and Media, April 9, 2020.</a></li>
<li><a href="https://www.cdc.gov/coronavirus/2019-ncov/specific-groups/high-risk-complications.html" target="_blank" rel="noreferrer noopener">“People Who Are at Higher Risk for Severe Illness.” Centers for Disease Control and Prevention, Accessed April 28, 2020.</a></li>
<li><a href="https://doi.org/10.3389/fpsyg.2016.00629" target="_blank" rel="noreferrer noopener">Losch, Eva, Maximilian D. Mühlberger MD, and Eva Jonas. “Comparing the Effectiveness of Individual Coaching, Self-Coaching, and Group Training: How Leadership Makes the Difference.” Frontiers in Psychology 7 (May 3, 2016).</a></li>
{{< /references >}}