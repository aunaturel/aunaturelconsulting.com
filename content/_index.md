---
title: Au Naturel Health Consulting Firm
date: 2020-08-31T20:25:29-04:00
draft: false
private: false
description: Au Naturel is a Physician-led health consulting firm that focuses
  on supporting professionals to achieve and maintain optimal health while
  balancing their hectic schedule and competing priorities.
image: /img/profile.png
imageposition: center
imagequality: 80
staticimage: false
calltitle: null
callbutton: null
calllink: null
---
{{< home/opener >}}

  <p>Busy, successful, driven but too busy taking care of everything and everyone else? Convinced it’s time to
      take care of the one thing that will help you enjoy your success, and help you remain productive?</p>
  <h3>We Understand</h3>
  <p>This is why at Au Naturel, we offer Professional Health Consulting that enables you to take charge of Your
      Health at Your Pace and in Your Time.</p>
{{< /home/opener >}}

{{< media-text class="secondary" image="/img/we-believe.jpg" title="We believe that you can be at your best, regardless of your age" button="Let's Get Started!" link="/sign-up/" >}}

<p>We specialize in creating evidence-based programs that are customized to improve health, and maximize physical and mental performance for the busy, professional.  Your personalized healing program will be individually crafted by a board-certified physician team with years of experience in successfully helping individuals of all ages, backgrounds, and disease history to not only improve, but often arrest and even reverse their chronic disease.</p>
{{< /media-text >}}

{{< home/packs top="You've probably heard that being healthy actually increases your productivity and saves time. But have you ever experienced this?" bottom="Download one of our Power Packed Cheat Sheets and Learn How!" >}}
  {{< home/pack image="/img/mask.jpg" title="Get started with Immune Boosting" button="I Need This!" link="/free-pdf/?s=immune-boosting" >}}7 Strategies to Boost Your Immune System and Fight COVID19{{< /home/pack >}}
  {{< home/pack image="/img/quick-thinking.jpg" title="Get started with Building Brain Health" button="Yes Please!" link="/free-pdf/?s=brain-health" >}}Think Quicker and Sharper, without adding A Minute To Your Day{{< /home/pack >}}
{{< /home/packs >}}

{{< media-text class="secondary" image="/img/schedule-call.jpg" crop="bottomleft" button="Schedule a Call Today!" link="/sign-up/" >}}

<p>At Au Naturel Health Consulting, we understand the pressures of being a professional in a busy world and we are here to help.  We understand that while education, career advancement, and wealth are all great accomplishments, too often they are obtained at the sacrifice of the one thing that will allow you to enjoy it-your health.</p>
{{< /media-text >}}

{{< home/testimonies >}}
{{< testimony name="Avis, Retired" title="The only program I suceeded on" description="Lost 16 pounds in 3 months" >}}
I’ve done weight watchers in the past and did great, however, this journey taught me HOW to eat. This is the only one I succeeded on.
{{< /testimony >}}

{{< testimony name="Novil, Retired Veteran" title="My PCP was astounded by my normalized bloodwork!" description="Lost 33 pounds in 3 months" >}}
My liver and kidney levels were horrifying and now I can say he (referring Primary Care Physician) is astounded by my normalized blood work… Once you invest the money, you don’t want to waste it.  Our biggest takeaway is that once you make your mind up to do something to improve yourself, you can do it!
{{< /testimony >}}

{{< testimony name="Michelle, Administrator" title="My doctor asked - What are you doing!" description="Lost 21 pounds in 4 months" >}}
When my doctor saw my cholesterol labs after only one month, he asked me - ‘What are you doing?!’.  He was so surprised and pleased to see such drastic improvements in my levels.
{{< /testimony >}}

{{< testimony name="Tonya, Teacher" title="My hands and feet aren't waking me up anymore!" description="Lost 21 pounds in 2.5 months" >}}
I said to myself ... something is different ... I don't have to hold on as much when I am walking … My hands and feet aren't waking me up during the middle of the night because of pain!
{{< /testimony >}}

{{< testimony name="Dr. Mandi, Practitioner" title="This is truly made for me" description="Lost 12 pounds in 6 weeks" >}}
As a wife, mother, professional, and entrepreneur, it’s easy to forget about oneself… Au Naturel took a holistic approach with an individualized assessment and an individualized plan that focused on my goals. The weekly check-ins and encouragement I received made me feel that this is truly made for me and I was not alone. It has been two months now and I am motivated to keep up with the plan that was developed for me, hoping to make a complete lifestyle change.
{{< /testimony >}}
{{< /home/testimonies >}}

{{< home/steps top="Ready to Get Started On Your Health Journey?" bottom="Follow These 3 Steps to Prioritizing You" button="Let's Journey Together!" link="/sign-up/" >}}
  {{< home/step image="/img/get-connected.jpg" title="Get Connected" >}}
  {{< home/step image="/img/get-evaluated.jpg" title="Get Evaluated" >}}
  {{< home/step image="/img/get-started.jpg" title="Get Started" >}}
{{< /home/steps >}}

{{< media-text class="dark" image="/img/community.jpg" crop="bottom" title="Our Promise To You and Our Community" button="Yes, Help Me, And Help Them!" link="/sign-up/" >}}

<p>As you take charge of your health, You will play a part in helping to change the lives of young women around the world. At Au Naturel, we are committed to making a tangible difference in the lives of the worlds most vulnerable. When you sign up for a program, a portion of the proceeds will go directly to help support females escaping human trafficking, so that they too can have an opportunity to improve their personal health- and their future.</p>

<p>Thank you for making a difference in lives all around the world.</p>
{{< /media-text >}}