---
draft: false
imageposition: null
imagequality: null
description: Improve Your Immune System and Increase Your Odds of Fighting Covid19
staticimage: true
title: Our Response to COVID19
date: 2020-06-21T19:06:08-04:00
image: /img/covid.png
private: false
---
The world is facing a pandemic that it has not seen before. The rise of cases in the Coronavirus instills fear in millions across the globe. While masses flock to buy hand sanitizers and toilet paper, many feel inept to proactively address and resist this virus if they are in fact brought in contact with it.

But we believe that there is HOPE and here’s why: According to the Centers of Disease and Prevention, people of any age who have serious underlying medical conditions may be at higher risk for more serious complications from COVID-19. In fact, the World Health Organization (WHO) states that, “For most people, COVID-19 infection will cause mild illness; however, it can make some people very ill and, in some people, it can be fatal…

Older people and those with pre-existing medical conditions (such as cardiovascular disease, chronic respiratory disease or diabetes) are at risk for severe disease” (2020). Statistics are quickly changing, and most recent observations show that “Covid-19 hits the old hardest, but young people are dying too” (Year, p. 3). It is clear that if there was ever a time to get serious about making a plan to address and improve our health , that time is NOW.

In response to this pandemic, our team has decided to launch a 14-Day COVID CHALLENGE. Anchored in the recommended self-quarantine time period, we’ve decided to offer a personalized and customized program designed to improve your personal health, with purpose and intentionality.

Imagine that during the next 14 days, instead of fearing the future in self-isolation, you could actually begin taking like changing steps to:

* Reverse Your Chronic Disease
* Improve your Overall Immunity
* Increase your chances of fighting disease

With the help of our staff, our goal is to help you do just that. In this special program you will work closely with our staff physicians to review your medical history, create a healing program, and get the support you need to be successful on that journey. Since this is a highly individualized program where you will be working closely with our physician staff, there are limited slots available

{{< button href="/sign-up/?i=14 Day Covid Challenge">}}Join the 14 Day Covid Challenge{{< /button >}}

{{< referenceslink >}}