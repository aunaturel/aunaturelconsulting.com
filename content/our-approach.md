---
title: Our Approach
date: 2020-06-24T21:38:09-04:00
draft: false
private: false
description: null
image: /img/untitled-design-6-.png
imageposition: right
imagequality: 70
calltitle: Because Your Healing Journey is Unique Lets work together to
  determine the level of support that you will need to succeed
callbutton: Begin Your Healing Journey
calllink: /sign-up/
---

Successful organizations spend millions of dollars on coaching and integrate this into their budgets – why? Because, as the Harvard Business Review simply puts it, **“Coaching Works”** Studies on coaching show that it is effective at reducing procrastination and facilitating goal attainment. Furthermore, leaders in innovation and strategy highlight that there is a growing body of empirical research showing that coaching really does facilitate goal achievement.

{{< parallax src="consultation.jpg" quality="70" position="left" >}}

At Au Naturel, we believe that your health is more important than the millions who work for in your organization, and that is why we are committed to protecting your most important asset, **your health**.

{{< parallax src="pineapple.jpg" quality="70" position="right" >}}

Our professional team will work alongside with you to create and implement **a customized and personalized health care plan** that will help YOU achieve YOUR health goals.

As an introduction to our program, everyone will receive an **individual consultation** With our medical team to review your case and make recommendations on a program that will work best for you.

Based on your underlying conditions and health goals, our team will then determine which **level of support you will need** to help you achieve your health goals.

{{< parallax src="sliced-fruit.jpg" quality="70" >}}

Our programs have varying levels of offerings and support, but in general will:
* Review your Nutritional Habits and Health Profile
* Develop A Customized Healing Program
* Provide In Depth Education on the Implications of Health Habits on Your Health Journey
* Review your Nutrition, Symptoms and Progress
* Brainstorm and Address Emotions that may Help or Hinder you from Achieving Your Health Goals
* Coach you through Anything that May be Getting in the Way of your Success

{{< referenceslink >}}