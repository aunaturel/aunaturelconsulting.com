// Mobile menu
const mm = document.getElementById("main-menu")
const mmOpen = document.getElementById("mobile-menu-open")
const mmClose = document.getElementById("mobile-menu-close")
mmOpen.addEventListener("click", toggleMobileMenu)
mmClose.addEventListener("click", toggleMobileMenu)

function toggleMobileMenu(e) {
    e.preventDefault()
    const state = mm.classList.toggle("open")
    if (state) {
        disableScroll()
    } else {
        enableScroll()
    }
}

// Smooth Scrolling
let anchor = document.createElement("div")
anchor.id = "scroll-anchor"
document.body.appendChild(anchor)
document.querySelectorAll("a").forEach(a => {
    const href = a.getAttribute("href")
    if (!href) return

    const parts = href.split("#")
    if (href.length < 2 ||
        parts.length < 2 ||
        window.location.pathname != parts[0]
    ) return

    a.addEventListener("click", e => {
        e.preventDefault()
        const target = document.getElementById(parts[1])
        if (target) {
            anchor.style.position = "absolute"
            anchor.style.top = `${target.offsetTop - 50}px`
            anchor.scrollIntoView({
                behavior: "smooth",
                block: "start"
            })
        }
    })
})

// Lazy Loading
function lazyLoad(entries, observer) {
    for (const entry of entries) {
        if (!entry.isIntersecting) continue;
        observer.unobserve(entry.target)

        const img = new Image();
        const src = entry.target.dataset.src;
        img.onload = () => {
            entry.target.classList.add("loaded")
            setTimeout(() => entry.target.setAttribute("src", src), 200);
        };
        img.src = src;
    }
}
let observer = new IntersectionObserver(lazyLoad, {
    rootMargin: "50px 0px 0px 50px"
})
document.querySelectorAll("img").forEach(img => observer.observe(img))

// Modals
let modalCfg = {
    disableScroll: true,
}

const popupID = "popup"
const popupOpenKey = "popup-date"
const popupDate = document.getElementById(popupID).dataset.date
if (document.getElementById(popupID)) {
    const lastClosed = localStorage.getItem(popupOpenKey)
    if (lastClosed != popupDate) {
        const popupTrigger = document.createElement("a");
        popupTrigger.setAttribute("data-micromodal-trigger", popupID)
        popupTrigger.style.position = "absolute";
        popupTrigger.style.visibility = "hidden";
        document.documentElement.appendChild(popupTrigger)
        document.querySelectorAll(`#${popupID} a`).forEach(a => a.addEventListener("click", e => {
            localStorage.setItem(popupOpenKey, popupDate)
        }))

        modalCfg = Object.assign(modalCfg, { onClose: () => localStorage.setItem(popupOpenKey, popupDate) })

        let timeout = 30000
        if (document.querySelector(".home")) timeout = 15000
        setTimeout(() => MicroModal.show(popupID, modalCfg), timeout);
    }
}

MicroModal.init(modalCfg);


// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
const scrollKeys = {
    37: 1,
    38: 1,
    39: 1,
    40: 1
};

const preventDefault = e => e.preventDefault();

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

// modern Chrome requires { passive: false } when adding event
var supportsPassive = false;
try {
    window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
        get: function () {
            supportsPassive = true;
        }
    }));
} catch (e) { }

var wheelOpt = supportsPassive ? {
    passive: false
} : false;
var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

// call this to Disable
function disableScroll() {
    window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF
    window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
    window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile
    window.addEventListener('keydown', preventDefaultForScrollKeys, false);
}

// call this to Enable
function enableScroll() {
    window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.removeEventListener(wheelEvent, preventDefault, wheelOpt);
    window.removeEventListener('touchmove', preventDefault, wheelOpt);
    window.removeEventListener('keydown', preventDefaultForScrollKeys, false);
}

// Signup form
const form = document.querySelector("form")
if (form) {
    const pairs = window.location.search.slice(1).split("&")
    for (const p of pairs) {
        const [k, v] = p.split("=")
        switch (k) {
            case "i":
                document.querySelector(`input[name="Interest"]`).value = decodeURI(v)
                break;
            case "s":
                const select = document.querySelector(`select[name="PDF"]`)
                select.querySelectorAll("option").forEach(op => {
                    console.log(op.value, decodeURI(v))
                    if (op.value == decodeURI(v)) op.selected = true
                })
                break;
        }
    }

    form.addEventListener("submit", async e => {
        e.preventDefault()
        localStorage.setItem(popupOpenKey, popupDate)

        let ob = {}
        const data = new FormData(form)
        data.forEach((value, key) => ob[key] = value)

        form.querySelectorAll("label>*").forEach(el => el.disabled = true)

        const tasks = JSON.parse(form.dataset.tasks)

        try {
            let request = await fetch("/.netlify/functions/email", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    tasks: tasks,
                    data: ob
                }),
            })

            let response = await request.json()
            if (!response.ok) throw response.message

            window.location.href = window.location.protocol + "//" + window.location.host + window.location.pathname + "thank-you"
        } catch (error) {
            console.error(error)
            let message = document.createElement("p")
            message.classList.add("form_message")
            message.classList.add("error")
            message.innerText = "Something went wrong. Please try again later."
            form.insertAdjacentElement("beforebegin", message)
            form.querySelectorAll("label>*").forEach(el => el.disabled = false)
        }
    })
}