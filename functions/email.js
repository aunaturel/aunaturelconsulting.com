const https = require("https");

// Expect post data
// {
// 	tasks: ["admin", "client"],
// 	data: {}
// }

let status = {
	subscribed: undefined,
	emails: undefined
}

function checkDone(callback, error = "") {
	let done = true
	let ok = true
	for (const k in status) {
		if (status[k] === undefined) done = false
		if (status[k] === false) ok = false
	}
	if (!done) return
	if (!ok) return callback(null, {
		statusCode: 500,
		body: JSON.stringify({
			ok: false,
			message: error
		})
	})
	return callback(null, {
		statusCode: 200,
		body: JSON.stringify({
			ok: true
		})
	})
}

exports.handler = function (event, context, callback) {
	if (event.headers["content-type"] != "application/json") {
		return callback(null, {
			statusCode: 400,
			body: "Invalid Content-Type Header; Expected application/json"
		})
	}

	const post = JSON.parse(event.body)
	subscribe(post.data,
		response => {
			console.log(response)
			status.subscribed = true
			checkDone(callback)
		},
		error => {
			console.log(error)
			status.subscribed = false
			checkDone(callback, error)
		}
	)

	let messages = []
	for (const t of post.tasks) {
		switch (t) {
			case "admin":
				messages.push(newAdminMessage(post.data))
				break;
			case "client":
				messages.push(newClientMessage(post.data))
				break;
			case "pdf":
				messages.push(newPDFMessage(post.data))
				break;
		}
	}

	sendEmails({
			"Messages": messages
		},
		response => {
			status.emails = true
			checkDone(callback)
		},
		error => {
			status.emails = false
			checkDone(callback, error)
		}
	)
}

function newAdminMessage(data) {
	let body = "<table style='font-family:sans-serif;border-collapse:collapse;'><tbody>"
	for (const k in data) {
		body += `
			<tr>
				<th style="padding:10px;border-bottom:1px solid #ccc;text-align:left;">${k}</th>
				<td style="padding:10px;border-bottom:1px solid #ccc;text-align:left;">${data[k]}</td>
			</tr>
		`
	}
	body += "</tbody></table>"

	const to = {
		"Email": "connect@aunaturelconsulting.com",
		"Name": "Au Naturel"
	}

	return newMessage(to, "New Form Submission", "You have a new form submission", body)
}

function newPDFMessage(data) {
	let link = ""

	switch (data["PDF"]) {
		case "immune-boosting":
			link = "https://aunaturelconsulting.com/7-strategies-to-boost-your-immune-system.pdf"
			break;
		case "brain-health":
			link = "https://aunaturelconsulting.com/7-tips-to-improve-brain-health.pdf"
			break;
		default:
			return
	}

	const body = `
		<div style="font-family:sans-serif;">
			<h1 style="margin-bottom: 10px;">Thank You for Signing Up!</h1>
			<p style="margin:10px 0 20px;">Here's your free gift, let's Start Your Healing Journey!</p>
			<p><a href="${link}" style="color:#dabf8c;padding:10px 20px;border-radius:3px;border:2px solid #dabf8c;background:transparent;font-size:1.3em;font-weight:500;text-decoration:none">Download Now</a></p>
		</div>
	`

	const to = {
		"Name": data["Name"],
		"Email": data["Email"]
	}

	return newMessage(to, "Here's your free gift", "Click here to grab your free gift", body)
}

function newClientMessage(data) {
	const body = `
		<div style="font-family:sans-serif;">
			<h1 style="margin-bottom: 10px;">Thank You for Signing Up!</h1>
			<p style="margin-bottom:10px;">We will get back to you in 1-2 business days.</p>
		</div>
	`

	const to = {
		"Name": data["Name"],
		"Email": data["Email"]
	}

	return newMessage(to, "Thanks for signing up", "We will get back to you in 1-2 business days", body)
}

function newMessage(to, subject, textPart, htmlPart) {
	return {
		"From": {
			"Email": "connect@aunaturelconsulting.com",
			"Name": "Au Naturel"
		},
		"To": [to],
		"Subject": subject,
		"TextPart": textPart,
		"HTMLPart": htmlPart
	}
}

function sendEmails(messages, success, fail) {
	const auth = `${process.env.MJ_APIKEY_PUBLIC}:${process.env.MJ_APIKEY_PRIVATE}`;
	const buff = Buffer.from(auth, 'utf-8');
	const authB64 = buff.toString('base64');

	const options = {
		hostname: 'api.mailjet.com',
		port: 443,
		path: '/v3.1/send',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Basic ${authB64}`
		}
	}

	let response = ''
	const req = https.request(options, res => {
		res.on('data', d => response += d)
		res.on('end', () => success(response))
	})

	req.on('error', error => fail(error))
	req.write(JSON.stringify(messages))
	req.end()
}

function subscribe(data, success, fail) {
	const [FNAME, ...LNAME] = data["Name"].split(" ")
	const body = {
		email_address: data["Email"],
		status: "subscribed",
		merge_fields: {
			"FNAME": FNAME,
			"LNAME": LNAME.join(" ")
		}
	}

	const auth = `key:${process.env.MC_APIKEY}`;
	const buff = Buffer.from(auth, 'utf-8');
	const authB64 = buff.toString('base64');

	const options = {
		hostname: 'us10.api.mailchimp.com',
		port: 443,
		path: '/3.0/lists/5658b75bd6/members',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Basic ${authB64}`
		}
	}

	let response = ''
	const req = https.request(options, res => {
		res.on('data', d => response += d)
		res.on('end', () => success(response))
	})

	req.on('error', error => fail(error))
	req.write(JSON.stringify(body))
	req.end()
}